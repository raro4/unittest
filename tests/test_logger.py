from unittest import TestCase, main

from logging import (LogRecord, DEBUG, INFO, WARNING, ERROR, CRITICAL)

from pytracelog.logger.handlers import StdoutHandler, StderrHandler

strfmt = '[%(asctime)s] [%(name)s] [%(levelname)s] > %(message)s'
datefmt = '%d-%m-%Y %H:%M:%S'


class HandlersTest(TestCase):
    def setUp(self):
        self.stdout = StdoutHandler()
        self.stderr = StderrHandler()
        self.log_debug = LogRecord(name='__main__', level=DEBUG, pathname='logger.py', lineno=76, msg='test debug',
                                   exc_info=None, args=())
        self.log_info = LogRecord(name='__main__', level=INFO, pathname='logger.py', lineno=76, msg='test info',
                                  exc_info=None, args=())
        self.log_warning = LogRecord(name='__main__', level=WARNING, pathname='logger.py', lineno=76,
                                     msg='test warning', exc_info=None, args=())
        self.log_error = LogRecord(name='__main__', level=ERROR, pathname='logger.py', lineno=76, msg='test info',
                                   exc_info=None, args=())

        self.log_critical = LogRecord(name='__main__', level=CRITICAL, pathname='logger.py', lineno=76,
                                      msg='test warning', exc_info=None, args=())

    def test_stdout_true(self):
        self.assertTrue(self.stdout.error_record_filter(self.log_debug))
        self.assertTrue(self.stdout.error_record_filter(self.log_info))
        self.assertTrue(self.stdout.error_record_filter(self.log_warning))

    def test_stdout_false(self):
        self.assertFalse(self.stdout.error_record_filter(self.log_error))
        self.assertFalse(self.stdout.error_record_filter(self.log_critical))

    def test_stderr_true(self):
        self.assertTrue(self.stderr.error_record_filter(self.log_error))
        self.assertTrue(self.stderr.error_record_filter(self.log_critical))

    def test_stderr_false(self):
        self.assertFalse(self.stderr.error_record_filter(self.log_debug))
        self.assertFalse(self.stderr.error_record_filter(self.log_info))
        self.assertFalse(self.stderr.error_record_filter(self.log_warning))


if __name__ == '__main__':
    main()
